module.exports =
{
  future:
  {
    removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
    defaultLineHeights: true,
    standardFontWeights: true
  },
  purge: [],
  theme:
  {
    extend:
    {
      maxWidth:
      {
        '50px': '50px'
      },
      flex:
      {
        '110': '1 1 0'
      },
      boxShadow:
      {
        'bottom': '0 -1px #ddd inset'
      },
      letterSpacing:
      {
        'header': '0.4em'
      }
    }
  },
  variants: {},
  plugins: []
};