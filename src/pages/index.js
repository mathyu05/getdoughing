import React from "react";
import breadIllustration from "../images/bread.svg";

const Home = props =>
{
  return (
    <div>
      <header
        className = "shadow-bottom"
      >
        <div
          className = "flex items-center justify-between p-4"
        >
          <div
            className = "flex-110"
          >

          </div>
          <div
            className = "flex"
          >
            <div
              className = "relative mb-0"
            >
              <div
                className = "text-center"
              >
                <img
                  className = "max-w-50px mx-auto my-0"
                  src = { breadIllustration }
                  alt = "Bread"
                />
              </div>
            </div>
          </div>
          <div
            className = "flex-110"
          >

          </div>
        </div>
      </header>
      <main>
        <header
          className = "relative my-8"
        >
          <div
            className = "mx-auto px-6"
          >
            <div
              className = "text-center"
            >
              <h1
                className = "mb-2 font-mono font-medium text-xl uppercase tracking-header"
              >
                Breads
              </h1>
            </div>
          </div>
        </header>
        <div
          className = "mt-12"
        >
          <div
            className = "px-2 list-none"
          >
            <div
              className = "w-1/2 inline-block h-3 bg-gray-900"
            >

            </div>
            <div
              className = "w-1/2 inline-block h-3 bg-gray-900"
            >

            </div>
            <div
              className = "w-1/2 inline-block h-3 bg-gray-900"
            >

            </div>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Home;